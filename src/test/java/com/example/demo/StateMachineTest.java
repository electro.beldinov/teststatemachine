package com.example.demo;

import com.example.demo.state.machine.DocumentEvent;
import com.example.demo.state.machine.DocumentState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.test.StateMachineTestPlan;
import org.springframework.statemachine.test.StateMachineTestPlanBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class StateMachineTest {

    @Autowired
    private StateMachineFactory<DocumentState, DocumentEvent> stateMachineFactory;

    @Test
    public void testStart() throws Exception {
        StateMachine<DocumentState, DocumentEvent> stateMachine = stateMachineFactory.getStateMachine();
        StateMachineTestPlan<DocumentState, DocumentEvent> plan =
                StateMachineTestPlanBuilder.<DocumentState, DocumentEvent>builder()
                        .stateMachine(stateMachine)
                        .step().expectState(DocumentState.START)
                        .and()
                        .build();
        plan.test();
    }

    @Test
    public void testInit() throws Exception {
        StateMachine<DocumentState, DocumentEvent> stateMachine = stateMachineFactory.getStateMachine();
        StateMachineTestPlan<DocumentState, DocumentEvent> plan =
                StateMachineTestPlanBuilder.<DocumentState, DocumentEvent>builder()
                        .stateMachine(stateMachine)
                        .step().expectState(DocumentState.START)
                        .and()
                        .step().sendEvent(DocumentEvent.INIT).expectState(DocumentState.INIT)
                        .and()
                        .build();
        plan.test();
    }


}