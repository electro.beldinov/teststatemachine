package com.example.demo.state.machine;

public enum DocumentState {
    START, END,
    INIT, AWAITS_SIGNATURE, SIGNED, SENT_TO_ABS, ACCEPTED_IN_ABS, EXECUTED,
    NOT_ACCEPTED_IN_ABS,
    REMOVED,
    REJECTED
}
