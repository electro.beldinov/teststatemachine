package com.example.demo.state.machine;

import com.example.demo.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

@Component
public class DocumentGuardEvaluator implements Guard<DocumentState, DocumentEvent> {

    @Autowired
    private DocumentService documentService;

    @Override
    public boolean evaluate(StateContext<DocumentState, DocumentEvent> stateContext) {
        Long documentId = stateContext.getExtendedState().get("DOCUMENT_ID", Long.class);
        return documentService.isDocumentSigned(documentId);
    }

}
