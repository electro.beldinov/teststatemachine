package com.example.demo.state.machine;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class InMemoryStateMachinePersist implements StateMachinePersist<DocumentState, DocumentEvent, Long> {

    private final HashMap<Long, StateMachineContext<DocumentState, DocumentEvent>> contexts = new HashMap<>();

    @Override
    public void write(StateMachineContext<DocumentState, DocumentEvent> context, Long contextObj) {
        contexts.put(contextObj, context);
    }

    @Override
    public StateMachineContext<DocumentState, DocumentEvent> read(Long contextObj) {
        return contexts.get(contextObj);
    }
}
