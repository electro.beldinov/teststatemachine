package com.example.demo.state.machine;

import com.example.demo.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

@Component
public class DocumentStateMachinePersist implements StateMachinePersist<DocumentState, DocumentEvent, Long> {

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public void write(StateMachineContext<DocumentState, DocumentEvent> context, Long documentId) {
        documentRepository.saveDocumentState(documentId, context.getState());
    }

    @Override
    public StateMachineContext<DocumentState, DocumentEvent> read(Long documentId) {
        DocumentState documentState = documentRepository.getDocumentStateById(documentId);

        DefaultExtendedState extendedState = new DefaultExtendedState();
        extendedState.getVariables().put("DOCUMENT_ID", documentId);
        return new DefaultStateMachineContext<>(documentState, null, null, extendedState, null, String.valueOf(documentId));
    }

}
