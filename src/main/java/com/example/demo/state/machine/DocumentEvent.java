package com.example.demo.state.machine;

public enum DocumentEvent {
    INIT, SUBMIT_TO_SIGN, SIGN, SEND_TO_ABS, ACCEPT, REFUSE, EXECUTE, REJECT
}
