package com.example.demo.state.machine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DocumentStateMachineListener implements StateMachineListener<DocumentState, DocumentEvent> {

    @Override
    public void stateChanged(State<DocumentState, DocumentEvent> from, State<DocumentState, DocumentEvent> to) {
        log.info("State change to: " + to.getId());
    }

    @Override
    public void stateEntered(State<DocumentState, DocumentEvent> state) {

    }

    @Override
    public void stateExited(State<DocumentState, DocumentEvent> state) {

    }

    @Override
    public void eventNotAccepted(Message<DocumentEvent> message) {
    }

    @Override
    public void transition(Transition<DocumentState, DocumentEvent> transition) {

    }

    @Override
    public void transitionStarted(Transition<DocumentState, DocumentEvent> transition) {

    }

    @Override
    public void transitionEnded(Transition<DocumentState, DocumentEvent> transition) {

    }

    @Override
    public void stateMachineStarted(StateMachine<DocumentState, DocumentEvent> stateMachine) {

    }

    @Override
    public void stateMachineStopped(StateMachine<DocumentState, DocumentEvent> stateMachine) {

    }

    @Override
    public void stateMachineError(StateMachine<DocumentState, DocumentEvent> stateMachine, Exception e) {
    }

    @Override
    public void extendedStateChanged(Object o, Object o1) {

    }

    @Override
    public void stateContext(StateContext<DocumentState, DocumentEvent> stateContext) {

    }
}
