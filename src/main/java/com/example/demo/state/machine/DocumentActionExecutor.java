package com.example.demo.state.machine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DocumentActionExecutor implements Action<DocumentState, DocumentEvent> {

    @Override
    public void execute(StateContext<DocumentState, DocumentEvent> stateContext) {
        log.info(String.format("event: %s, documentId: %d", stateContext.getEvent().name(), stateContext.getExtendedState().get("DOCUMENT_ID", Long.class)));
    }
}
