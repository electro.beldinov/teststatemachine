package com.example.demo.config;

import com.example.demo.state.machine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.guard.Guard;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;

import java.util.EnumSet;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<DocumentState, DocumentEvent> {

    @Autowired
    private DocumentStateMachineListener stateMachineListener;

    @Autowired
    private DocumentActionExecutor actionExecutor;

    @Autowired
    private DocumentGuardEvaluator guardEvaluator;

    @Autowired
    private DocumentStateMachinePersist stateMachinePersist;

    @Override
    public void configure(StateMachineConfigurationConfigurer<DocumentState, DocumentEvent> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(false)
                .listener(listener());
    }

    @Override
    public void configure(StateMachineStateConfigurer<DocumentState, DocumentEvent> states)
            throws Exception {
        states
                .withStates()
                .initial(DocumentState.START)
                .end(DocumentState.END)
                .states(EnumSet.allOf(DocumentState.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<DocumentState, DocumentEvent> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(DocumentState.START).target(DocumentState.INIT).event(DocumentEvent.INIT).and()
                .withExternal()
                .source(DocumentState.INIT).target(DocumentState.AWAITS_SIGNATURE).event(DocumentEvent.SUBMIT_TO_SIGN).and()
                .withExternal()
                .source(DocumentState.AWAITS_SIGNATURE).target(DocumentState.SIGNED).event(DocumentEvent.SIGN).guard(evaluate()).and()
                .withExternal()
                .source(DocumentState.SIGNED).target(DocumentState.SENT_TO_ABS).event(DocumentEvent.SEND_TO_ABS).action(executeAction()).and()
                .withFork()
                .source(DocumentState.SENT_TO_ABS).target(DocumentState.ACCEPTED_IN_ABS).target(DocumentState.NOT_ACCEPTED_IN_ABS).and()
                .withExternal()
                .source(DocumentState.SENT_TO_ABS).target(DocumentState.ACCEPTED_IN_ABS).event(DocumentEvent.ACCEPT).and()
                .withExternal()
                .source(DocumentState.SENT_TO_ABS).target(DocumentState.NOT_ACCEPTED_IN_ABS).event(DocumentEvent.REFUSE).and()
                .withFork()
                .source(DocumentState.ACCEPTED_IN_ABS).target(DocumentState.EXECUTED).target(DocumentState.REJECTED).and()
                .withExternal()
                .source(DocumentState.ACCEPTED_IN_ABS).target(DocumentState.EXECUTED).event(DocumentEvent.EXECUTE).and()
                .withExternal()
                .source(DocumentState.ACCEPTED_IN_ABS).target(DocumentState.REJECTED).event(DocumentEvent.REJECT).and();
    }

    @Bean
    public StateMachineListener<DocumentState, DocumentEvent> listener() {
        return stateMachineListener;
    }

    @Bean
    public Action<DocumentState, DocumentEvent> executeAction() {
        return actionExecutor;
    }

    @Bean
    public Guard<DocumentState, DocumentEvent> evaluate() {
        return guardEvaluator;
    }

    @Bean
    public StateMachinePersister<DocumentState, DocumentEvent, Long> persister() {
        return new DefaultStateMachinePersister<>(stateMachinePersist);
    }

}
