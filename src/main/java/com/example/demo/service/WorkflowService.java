package com.example.demo.service;

import com.example.demo.state.machine.DocumentEvent;
import com.example.demo.state.machine.DocumentState;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class WorkflowService {

    @Autowired
    private StateMachineFactory<DocumentState, DocumentEvent> stateMachineFactory;

    @Autowired
    private StateMachinePersister<DocumentState, DocumentEvent, Long> persister;

    @PostConstruct
    public void test() {
        Long documentId = 34543L;
        documentEvent(documentId, DocumentEvent.INIT);
        documentEvent(documentId, DocumentEvent.SUBMIT_TO_SIGN);
        documentEvent(documentId, DocumentEvent.SIGN);
        documentEvent(documentId, DocumentEvent.SEND_TO_ABS);
        documentEvent(documentId, DocumentEvent.ACCEPT);
        documentEvent(documentId, DocumentEvent.EXECUTE);
        log.info("______________________________________");

        documentId = 23445L;
        documentEvent(documentId, DocumentEvent.INIT);
        documentEvent(documentId, DocumentEvent.SUBMIT_TO_SIGN);
        documentEvent(documentId, DocumentEvent.SIGN);
        documentEvent(documentId, DocumentEvent.SEND_TO_ABS);
        documentEvent(documentId, DocumentEvent.ACCEPT);
        documentEvent(documentId, DocumentEvent.EXECUTE);
        log.info("______________________________________");
    }

    @SneakyThrows
    public void documentEvent(Long documentId, DocumentEvent documentEvent) {
        StateMachine<DocumentState, DocumentEvent> stateMachine = stateMachineFactory.getStateMachine();
        persister.restore(stateMachine, documentId);
        stateMachine.start();

        Message<DocumentEvent> eventMessage = MessageBuilder.withPayload(documentEvent)
                .setHeader("DOCUMENT_ID", documentId)
                .build();
        stateMachine.sendEvent(eventMessage);
        persister.persist(stateMachine, documentId);
    }

}
