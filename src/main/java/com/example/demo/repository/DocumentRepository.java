package com.example.demo.repository;

import com.example.demo.state.machine.DocumentState;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class DocumentRepository {

    private final HashMap<Long, DocumentState> db = new HashMap<>();

    public DocumentState saveDocumentState(Long documentId, DocumentState documentState) {
        return db.put(documentId, documentState);
    }

    public DocumentState getDocumentStateById(Long documentId) {
        return db.get(documentId);
    }

}
